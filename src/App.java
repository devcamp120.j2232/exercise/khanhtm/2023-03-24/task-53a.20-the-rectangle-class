public class App {
    public static void main(String[] args) throws Exception {
        //Khởi tạo đối tượng hình chữ nhật rectangle1 (không có tham số)
        Rectangle rectangle1 = new Rectangle();
        //Khởi tạo đối tượng hình chữ nhật rectangle1 (có tham số)
        Rectangle rectangle2 = new Rectangle(3.0f, 2.0f);
        System.out.println(rectangle1.toString());
        System.out.println(rectangle2.toString());
        //Lấy diện tich
        double dientich1 = rectangle1.getArea();
        double dientich2 = rectangle1.getArea();
        //Lấy chu vi
        double chuvi1 = rectangle1.getPerimeter();
        double chuvi2 = rectangle1.getPerimeter();
        System.out.println("Diện tích của đối tượng rectangle1 = " + dientich1 + ", chu vi = " + chuvi1);
        System.out.println("Diện tích của đối tượng rectangle2 = " + dientich2 + ", chu vi = " + chuvi2);
    }
}
