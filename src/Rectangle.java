public class Rectangle {
 /**
  *  Thuộc tính chiều dài (length)
  */
  private float length = 1.0f;
  /**
  *  Thuộc tính chiều rộng (width)
  */
  private float width = 1.0f;
  /**
  *  phương thức khởi tạo không có tham số
  */
  public Rectangle() {
   super();
  }
  /**
  *  phương thức khởi tạo có tham số
  */
  public Rectangle(float length, float width) {
   super();
   this.length = length;
   this.width = width;
  }
  /**
  *  getter method
  */
  public float getLength() {
   return this.length;
  }
  /**
  *  setter method
  *  @param length
  */
  public void setLength(float length) {
   this.length = length;
  }
  /**
  *  getter method
  */
  public float getWidth() {
   return width;
  }
  /**
  *  setter method
  *  @param width
  */
  public void setWidth(float width) {
   this.width = width;
  }
  /**
  *  phương thúc tính diện tich chữ nhật
  * @return
  */
  public float getArea() {
   return length * width;
  }
  /**
  *  phương thúc tính diện tich chu vi
  * @return
  */
  public float getPerimeter() {
   return 2 * (length + width);
  }

  /**
   * In ra console
   */
  public String toString(){
   return String.format("Rectangle [length= %s, width= %s]", length, width);
  }
}
